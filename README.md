Galen Framework PoC
=====================================

This project is used in order to demonstrate the features of [Galen Framework](http://galenframework.com)

Sample tests will be executed on Consumer Portal Home page (Staging env)


Prerequisites
=====================================

- Java v1.8+
- NodeJS (npm installation)
- Git
- Firefox & Chrome browsers


Galen Installation (macOS & Linux)
=====================================

NPM Installation:
-------------------------------------
```
sudo npm install -g galenframework-cli
```

If you have issues installing Galen via npm please download the [archive](http://galenframework.com/download/), extract it and execute the following command via Terminal:
-------------------------------------
```
sudo ./install.sh
```

To check whether Galen was successfully installed please run the following command in your Terminal:
-------------------------------------
```
galen -v
```


Running Tests
=====================================

```
git clone https://stanislavemelianov@bitbucket.org/stanislavemelianov/galen_poc.git
cd galen_poc/
```

In order to run a single test please use the following command:

```
galen check header.gspec --url "https://test-www.quandoo.de/en" --size "1280x720" --include "desktop,all"
```

In order to run a test suite:

```
galen test test/homepage_suite.test --htmlreport reports
```